package app

import groovy.sql.Sql
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.hive.ql.exec.vector.BytesColumnVector
import org.apache.hadoop.hive.ql.exec.vector.ColumnVector
import org.apache.hadoop.hive.ql.exec.vector.DecimalColumnVector
import org.apache.hadoop.hive.ql.exec.vector.DoubleColumnVector
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector
import org.apache.hadoop.hive.ql.exec.vector.TimestampColumnVector
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch
import org.apache.hadoop.hive.ql.io.orc.OrcFile
import org.apache.hadoop.hive.serde2.io.HiveDecimalWritable
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector
import org.apache.hadoop.hive.serde2.typeinfo.BaseCharTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.DecimalTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.ListTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.MapTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.PrimitiveTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.StructTypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils
import org.apache.hadoop.hive.serde2.typeinfo.UnionTypeInfo
import org.apache.hadoop.hive.ql.io.orc.CompressionKind
import org.apache.orc.TypeDescription
import org.apache.orc.Writer

class WriteToOrcFileFromDataBase {
    Path filePath
    Configuration conf

    String sqlServer
    String sqlDatabase
    String sqlUser
    String sqlPassword
    String sqlDriver
    String sqlQuery

    void createOrcFile() {
        def sql = Sql.newInstance("jdbc:sqlserver://$sqlServer;DatabaseName=$sqlDatabase;", "$sqlUser", "$sqlPassword",
                "$sqlDriver")

        String sqlTableNameAndType = "SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM ${sqlDatabase}.INFORMATION_SCHEMA.COLUMNS" +
                " WHERE Table_name = '${takingTableFromSqlQuery(sqlQuery)}'"

        String columnsFromSqlQuery = takingColumnsFromSqlQuery(sqlQuery)
        boolean notCheckColumns = columnsFromSqlQuery.contains("*") ? true : false

        Map columnNameTypeMap = createColumnNameTypeMap(sql, sqlTableNameAndType, notCheckColumns, columnsFromSqlQuery)

        TypeDescription schema = TypeDescription.createStruct()
        createSchema(schema, columnNameTypeMap)

        Writer writer = OrcFile.createWriter(filePath, OrcFile.writerOptions(conf).setSchema(schema).compress(CompressionKind.ZLIB))
        VectorizedRowBatch batch = schema.createRowBatch()
        fillingTheBatch(sql, sqlQuery, writer, batch, columnNameTypeMap.size() - 1)

        if (batch.size != 0)
            addBatchToWriterAndResetBatch(writer, batch)
        writer.close()
    }

    private String takingColumnsFromSqlQuery(String sqlQuery) {
        return " " + sqlQuery.split("(?i) from ")[0].split("(?i)select ")[1]
                .replaceAll(/(?i)top\([0-9]+\)/, "").replaceAll(/\n/, " ")
                .replace(",", " ").replaceAll(/\s{2,}/, " ")
                .replace("[", "").replace("]", "").trim() + " "
    }

    private String takingTableFromSqlQuery(String sqlQuery) {
        return sqlQuery.split("(?i) from ")[1].split("(?i) where ")[0].trim()
                .replace("[", "").replace("]", "")
    }

    private Map createColumnNameTypeMap(
            def sql, String sqlTableNameAndType, boolean notCheckColumns, String columnsFromSqlQuery) {

        Map tempMap = [:]

        sql.eachRow(sqlTableNameAndType) { row ->
            String columnName = row.column_name

            if (notCheckColumns || columnsFromSqlQuery.contains(" " + columnName + " ")) {
                String dataType = row.data_type
                def dataTypeLength = row.CHARACTER_MAXIMUM_LENGTH

                tempMap.put(columnName, reformatDataType(dataType, dataTypeLength))
            }
        }

        return tempMap
    }

    private String createColumnNameTypeStringStruct(Map columnNameTypeMap) {
        return (columnNameTypeMap as String)
                .replace("[", "struct<").replace("]", ">").replaceAll(/\s*/, "")
    }

    private void createSchema(TypeDescription schema, Map columnNameTypeMap) {
        TypeInfo typeInfo = TypeInfoUtils.getTypeInfoFromTypeString(createColumnNameTypeStringStruct(columnNameTypeMap))
        StructTypeInfo sinfo = (StructTypeInfo) typeInfo

        for (String fieldName : sinfo.getAllStructFieldNames()) {
            schema.addField(fieldName, convertTypeInfo(sinfo.getStructFieldTypeInfo(fieldName)))
        }
    }

    private String reformatDataType(String dataType, def dataTypeLength) {
        if (dataType.contains("varchar")) {
            int varcharMaxSize = dataTypeLength == -1 ? 8000 : dataType.contains("nvarchar") ? (dataTypeLength) * 2 : dataTypeLength
            return "varchar($varcharMaxSize)"
        }

        if (dataType.contains("datetime")) {
            return "timestamp"
        }

        if (dataType.equals("bit")) {
            return "boolean"
        }

        if (dataType.equals("uniqueidentifier")) {
            return "string"
        }

        if (dataType.equals("money")) {
            return "double"
        }

        if (dataType.equals("date")) {
            return "string"
        }

        return dataType
    }

    private void fillingTheBatch(def sql, String sqlQuery, Writer writer, VectorizedRowBatch batch, int columnsLength) {
        sql.eachRow(sqlQuery) { row ->
            int v = batch.size

            (0..columnsLength).each { i ->
                if (row[i] == null) {
                    batch.cols[i].isNull[v] = true
                    batch.cols[i].noNulls = false
                    return
                }

                addToBatch(batch.cols[i], row[i], v)
            }

            ++batch.size

            if (batch.size == batch.getMaxSize())
                addBatchToWriterAndResetBatch(writer, batch)
        }
    }

    private void addToBatch(ColumnVector column, def row, int v) {
        if (column instanceof BytesColumnVector) {
            ((BytesColumnVector) column).setVal(v, row.toString().getBytes())
            return
        }

        if (column instanceof DecimalColumnVector) {
            ((DecimalColumnVector) column).vector[v] = new HiveDecimalWritable(row)
            return
        }

        if (column instanceof DoubleColumnVector) {
            ((DoubleColumnVector) column).vector[v] = Double.valueOf(row)
            return
        }

        if (column instanceof LongColumnVector && (row == true || row == false)) {
            ((LongColumnVector) column).vector[v] = row ? 1 : 0
            return
        }

        if (column instanceof LongColumnVector) {
            ((LongColumnVector) column).vector[v] = row
            return
        }

        if (column instanceof TimestampColumnVector) {
            ((TimestampColumnVector) column).set(v, row)
            return
        }
    }

    private void addBatchToWriterAndResetBatch(Writer writer, VectorizedRowBatch batch) {
        writer.addRowBatch(batch)
        batch.reset()
    }

    private TypeDescription convertTypeInfo(TypeInfo info) {
        switch (info.getCategory()) {
            case ObjectInspector.Category.PRIMITIVE:
                PrimitiveTypeInfo pinfo = (PrimitiveTypeInfo) info
                switch (pinfo.getPrimitiveCategory()) {
                    case PrimitiveObjectInspector.PrimitiveCategory.BOOLEAN:
                        return TypeDescription.createBoolean()
                    case PrimitiveObjectInspector.PrimitiveCategory.BYTE:
                        return TypeDescription.createByte()
                    case PrimitiveObjectInspector.PrimitiveCategory.SHORT:
                        return TypeDescription.createShort()
                    case PrimitiveObjectInspector.PrimitiveCategory.INT:
                        return TypeDescription.createInt()
                    case PrimitiveObjectInspector.PrimitiveCategory.LONG:
                        return TypeDescription.createLong()
                    case PrimitiveObjectInspector.PrimitiveCategory.FLOAT:
                        return TypeDescription.createFloat()
                    case PrimitiveObjectInspector.PrimitiveCategory.DOUBLE:
                        return TypeDescription.createDouble()
                    case PrimitiveObjectInspector.PrimitiveCategory.STRING:
                        return TypeDescription.createString()
                    case PrimitiveObjectInspector.PrimitiveCategory.DATE:
                        return TypeDescription.createDate()
                    case PrimitiveObjectInspector.PrimitiveCategory.TIMESTAMP:
                        return TypeDescription.createTimestamp()
                    case PrimitiveObjectInspector.PrimitiveCategory.BINARY:
                        return TypeDescription.createBinary()
                    case PrimitiveObjectInspector.PrimitiveCategory.DECIMAL:
                        DecimalTypeInfo dinfo = (DecimalTypeInfo) pinfo
                        return TypeDescription.createDecimal()
                                .withScale(dinfo.getScale())
                                .withPrecision(dinfo.getPrecision())
                    case PrimitiveObjectInspector.PrimitiveCategory.VARCHAR:
                        BaseCharTypeInfo cinfo = (BaseCharTypeInfo) pinfo
                        return TypeDescription.createVarchar()
                                .withMaxLength(cinfo.getLength())
                    case PrimitiveObjectInspector.PrimitiveCategory.CHAR:
                        BaseCharTypeInfo cinfo = (BaseCharTypeInfo) pinfo
                        return TypeDescription.createChar()
                                .withMaxLength(cinfo.getLength())
                    default:
                        throw new IllegalArgumentException("ORC doesn't handle primitive" +
                                " category " + pinfo.getPrimitiveCategory())
                }
            case ObjectInspector.Category.LIST:
                ListTypeInfo linfo = (ListTypeInfo) info
                return TypeDescription.createList(convertTypeInfo(linfo.getListElementTypeInfo()))
            case ObjectInspector.Category.MAP:
                MapTypeInfo minfo = (MapTypeInfo) info
                return TypeDescription.createMap(convertTypeInfo(minfo.getMapKeyTypeInfo()),
                        convertTypeInfo(minfo.getMapValueTypeInfo()))
            case ObjectInspector.Category.UNION:
                UnionTypeInfo minfo = (UnionTypeInfo) info
                TypeDescription result = TypeDescription.createUnion()
                for (TypeInfo child : minfo.getAllUnionObjectTypeInfos()) {
                    result.addUnionChild(convertTypeInfo(child))
                }
                return result
            case ObjectInspector.Category.STRUCT:
                StructTypeInfo sinfo = (StructTypeInfo) info
                TypeDescription result = TypeDescription.createStruct()
                for (String fieldName : sinfo.getAllStructFieldNames()) {
                    result.addField(fieldName,
                            convertTypeInfo(sinfo.getStructFieldTypeInfo(fieldName)))
                    return result
                }
            default:
                throw new IllegalArgumentException("ORC doesn't handle " +
                        info.getCategory())
        }
    }
}
