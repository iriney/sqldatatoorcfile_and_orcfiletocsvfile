package app

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.yaml.snakeyaml.Yaml


class Main {

    static void main(String[] args) {
        Map yml = new Yaml().load(new FileReader("D://workspace//dataFromQueryToOrcFile/src/main/resources/application-dev.yml"))
        String sqlServer = yml.get("sqlServer")
        String sqlUser = yml.get("sqlUser")
        String sqlPassword = yml.get("sqlPassword")

        String sqlDriver = 'com.microsoft.sqlserver.jdbc.SQLServerDriver'

        String orcFilePath = new Path("D://workspace//dataFromQueryToOrcFile/product.orc")
        String csvFilePath = "D://workspace//dataFromQueryToOrcFile/output.csv"
        Configuration conf = new Configuration()

//      database can be different
        String sqlDatabase = 'TWIQINSIDER'

//      the table and columns can be different
        String sqlQuery = "select top(15000) * From [Transaction]"

//      write to orc file the data from sql query
        deleteFileIfExist(orcFilePath)
        WriteToOrcFileFromDataBase writeToOrcFileFromDataBase = new WriteToOrcFileFromDataBase("filePath": new Path(orcFilePath), "conf": conf, "sqlServer": sqlServer,
                "sqlDatabase": sqlDatabase, "sqlUser": sqlUser, "sqlPassword": sqlPassword, "sqlDriver": sqlDriver, "sqlQuery": sqlQuery)
        writeToOrcFileFromDataBase.createOrcFile()

//      read from orc file and create csv file
        ReadFromOrcFileAndWriteToCsvFile readFromOrcFileAndWriteToCsvFile = new ReadFromOrcFileAndWriteToCsvFile("orcFilePath": new Path(orcFilePath), "conf": conf,
                "csvFilePath": csvFilePath)
        readFromOrcFileAndWriteToCsvFile.createCsvFileFromOrcFile()
    }

    private static void deleteFileIfExist(String path) {
        File file = new File(path)
        if (file.exists()) {
            file.delete()
        }
    }
}
