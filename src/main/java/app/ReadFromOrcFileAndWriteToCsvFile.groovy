package app

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.hive.ql.io.orc.OrcFile
import org.apache.hadoop.hive.ql.io.orc.Reader
import org.apache.hadoop.hive.ql.io.orc.RecordReader
import org.apache.hadoop.hive.serde2.objectinspector.StructField
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector

class ReadFromOrcFileAndWriteToCsvFile {
    Configuration conf
    Path orcFilePath
    String csvFilePath

    void createCsvFileFromOrcFile() {
        FileSystem fs = orcFilePath.getFileSystem(conf)
        Reader reader = OrcFile.createReader(fs, orcFilePath)

        StringBuilder builder = new StringBuilder()
        StructObjectInspector inspector = (StructObjectInspector) reader.getObjectInspector()

        generateCsvFile(builder, inspector, reader)
    }

    private void generateCsvFile(StringBuilder builder, StructObjectInspector inspector, Reader reader) {
        def row = null
        RecordReader records = reader.rows()

        addingColumnsNamesToCsv(builder, inspector)

        while (records.hasNext()) {
            row = records.next(row)

            addingColumnsDataToCsv(builder, inspector.getStructFieldsDataAsList(row) as List)

            if (records.hasNext())
                builder.append('\n')
        }

        new File(csvFilePath).write(builder.toString())
    }

    private void addingColumnsNamesToCsv(StringBuilder stringBuilder, StructObjectInspector structObjectInspector) {
        structObjectInspector.getAllStructFieldRefs().each { it ->
            if (it != structObjectInspector.getAllStructFieldRefs().last()) {
                stringBuilder.append(((StructField) it).getFieldName().toString() + ',')
                return
            }

            stringBuilder.append(((StructField) it).getFieldName().toString())
        }
        stringBuilder.append('\n')
    }

    private void addingColumnsDataToCsv(StringBuilder stringBuilder, List listWithColumnsData) {
        listWithColumnsData.eachWithIndex { def field, int index ->
            if (field != null) {
                stringBuilder.append(reformatString(field.toString()))
            } else {
                stringBuilder.append("NULL")
            }

            if (index != listWithColumnsData.size() - 1)
                stringBuilder.append(',')
        }
    }

    private String reformatString(String field) {
        field = field.replaceAll(/[\n\r]/, " ")
        field = field.contains(",") ? "\"" + field + "\"" : field == "true" ? "1" : field == "false" ? "0" : field
        return field
    }
}
